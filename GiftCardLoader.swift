//
//  GiftCardLoader.swift
//  GiftCard
//
//  Created by Qi Xiong Ng on 23/04/2016.
//  Copyright © 2016 Qi Xiong Ng. All rights reserved.
//

import UIKit

class GiftCardLoader {
    static let sharedLoader: GiftCardLoader = GiftCardLoader()
    private init () {}
    
    var giftCards: [GiftCard] = []
    let fileManager: NSFileManager = NSFileManager.defaultManager()
    
    func dataFileURL() -> NSURL {
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        
        return filePath
    }
    
    func readGiftCardsFromFile() -> [GiftCard] {
        let giftCardsData: [NSData] = NSArray(contentsOfURL: self.dataFileURL()) as! [NSData]
        var giftCards: [GiftCard] = []
        
        for data in giftCardsData {
            let giftCard: GiftCard = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! GiftCard
            giftCards.append(giftCard)
        }
        
        self.giftCards = giftCards
        return giftCards
    }
    
    
    func saveGiftCardsToFile () {
        var giftCardsData: [NSData] = []
        for giftCard in giftCards {
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(giftCard)
            giftCardsData.append(data)
            
        }
        
        (giftCardsData as NSArray).writeToURL(self.dataFileURL(), atomically: true)
        
    }
    
    func saveEditedGiftCardsToFile () {
        var giftCardsData: [NSData] = []
        for giftCard in giftCards {
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(giftCard)
            giftCardsData.append(data)
            
        }
        
        (giftCardsData as NSArray).writeToURL(self.dataFileURL(), atomically: true)
    }
    
//    func addNewGiftCard(giftcard: GiftCard) {
//        giftCards.append(giftcard)
//        saveGiftCardsToFile(giftcard)
//        }
//    
//    func editGiftCard(giftcard: GiftCard) {
//        saveGiftCardsToFile(giftcard)
//        }
}



