//
//  GiftCardPersistenceTests.swift
//  GiftCard
//
//  Created by Qi Xiong Ng on 23/04/2016.
//  Copyright © 2016 Qi Xiong Ng. All rights reserved.
//

import XCTest
@testable import GiftCard

class GiftCardPersistenceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testWritingToFile() {
        var giftCards: [GiftCard] = []
        let giftCard: GiftCard = GiftCard(giftCardNumber: "9023123412095746", pinNumber: "4012", currentBalance: 100.0, storeName: "Groupon")
        let giftCard2: GiftCard = GiftCard(giftCardNumber: "9012930412421049", pinNumber: "0920", currentBalance: 90.0, storeName: "Parkson")
        let giftCard3: GiftCard = GiftCard(giftCardNumber: "0329320129392303", pinNumber: "9023", currentBalance: 80.0, storeName: "Groupon")
        giftCards = [giftCard, giftCard2, giftCard3]
    
        GiftCardLoader.sharedLoader.saveGiftCardsToFile()
        let filePath: NSURL = GiftCardLoader.sharedLoader.dataFileURL()
        
        let fileExists: Bool = NSFileManager.defaultManager().fileExistsAtPath(filePath.path!)
        
//        let pathFile: NSURL = GiftCardLoader.sharedLoader
//        GiftCardLoader.sharedLoader.giftCards[0].pinNumber = "sdf"
//        GiftCardLoader.sharedLoader.giftCards.append(GiftCard())
//        GiftCardLoader.sharedLoader.saveGiftCardsToFile()
        
//        var fileExists: Bool = fileManager.fileExistsAtPath(filePath.path!)
        XCTAssert(fileExists, "File should exists after write")
        

    }
    
    func testReadingFromFile(){
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        let giftCardsData: [NSData] = NSArray(contentsOfURL: filePath) as! [NSData]
        var giftCards: [GiftCard] = []
        
        for data in giftCardsData {
            let giftCard: GiftCard = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! GiftCard
            giftCards.append(giftCard)
        }
        
        XCTAssert(giftCards.count == 3, "Should have loaded 3 giftCards from file")
        
    }
    
    
    func testSavingGiftCard() {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        var giftCards: [GiftCard] = []
        let giftCard: GiftCard = GiftCard(giftCardNumber: "9023123412095746", pinNumber: "4012", currentBalance: 100.0, storeName: "Groupon")
        let giftCard2: GiftCard = GiftCard(giftCardNumber: "9012930412421049", pinNumber: "0920", currentBalance: 90.0, storeName: "Parkson")
        let giftCard3: GiftCard = GiftCard(giftCardNumber: "0329320129392303", pinNumber: "9023", currentBalance: 80.0, storeName: "Groupon")
        giftCards = [giftCard, giftCard2, giftCard3]
        
        var giftCardsData: [NSData] = []
        for giftCard in giftCards {
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(giftCard)
            giftCardsData.append(data)
            
        }
        
        defaults.setObject(giftCardsData, forKey: "testgiftCardData")
        defaults.synchronize()
    
        
    }
    
    func testReadingGiftCard() {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var readGiftCardData: [NSData] = defaults.objectForKey("testgiftCardData") as! [NSData]
        var giftCards: [GiftCard] = []
        
        for data in readGiftCardData {
            let readGiftCard: GiftCard? = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? GiftCard
            giftCards.append(readGiftCard!)
        }
        
//        XCTAssertNotNil(readGiftCard, "Should be able to read giftCard data from defaults and return and object")
//        XCTAssertEqual(giftCard.giftCardNumber, readGiftCard!.giftCardNumber, "Names from giftCard and recreated giftCard should match")
        XCTAssert(giftCards.count > 0, "There should be at least a gift card")
        
    }
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
