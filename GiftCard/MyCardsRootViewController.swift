//
//  myCardsRootViewController.swift
//  GiftCard
//
//  Created by Qi Xiong Ng on 4/2/16.
//  Copyright © 2016 Qi Xiong Ng. All rights reserved.
//

import UIKit

class MyCardsRootViewController: UIViewController {

    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var giftCards: [GiftCard] = GiftCardLoader.sharedLoader.giftCards
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }

    @IBAction func addNewCard(sender: AnyObject) {
        let addView: AddEditGiftCardViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddEditGiftCardViewController") as! AddEditGiftCardViewController
        addView.delegate = self
        self.presentViewController(addView, animated: true, completion: nil)
        
    }

}

extension MyCardsRootViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return giftCards.count
    }
 
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: GiftCardTableViewCell = tableView.dequeueReusableCellWithIdentifier("GiftCardTableViewCell", forIndexPath: indexPath) as! GiftCardTableViewCell
        cell.giftCard = giftCards[indexPath.row]
        return cell
    }
    

}

extension MyCardsRootViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
        let showView: ShowGiftCardDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ShowGiftCardDetailsViewController") as! ShowGiftCardDetailsViewController
        showView.giftCard = giftCards[indexPath.row]

        self.navigationController?.pushViewController(showView, animated: true)
    print ("User selected card \(indexPath.row)")
    }

    
    func tableView(tableView: UITableView,
        accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
            
    }
}

extension MyCardsRootViewController: AddEditGiftCardViewControllerDelegate {
    
    func userDidSave(giftcard: GiftCard) {
        giftCards.append(giftcard)
        tableView.reloadData()

        // save to disk
//        GiftCardLoader.sharedLoader.addNewGiftCard(giftcard)
        GiftCardLoader.sharedLoader.giftCards = giftCards
        GiftCardLoader.sharedLoader.saveGiftCardsToFile()
    }
}