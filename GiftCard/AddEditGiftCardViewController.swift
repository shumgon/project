//
//  addViewController.swift
//  GiftCard
//
//  Created by Qi Xiong Ng on 4/2/16.
//  Copyright © 2016 Qi Xiong Ng. All rights reserved.
//

import UIKit

protocol AddEditGiftCardViewControllerDelegate {
    func userDidSave(giftcard:GiftCard)
    
}
class AddEditGiftCardViewController: UIViewController {
    var editingGiftCard: GiftCard?
    var delegate: AddEditGiftCardViewControllerDelegate?
    var pickerData: [String] = ["Starbucks", "Parkson", "Groupon"]
    
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var giftCardNumberTextField: UITextField!
    @IBOutlet weak var pinNumberTextField: UITextField!
    @IBOutlet weak var currentBalanceTextField: UITextField!
    @IBOutlet weak var storeNameTextField: UITextField!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.storeNameTextField.inputView
        
        
        if let giftCard: GiftCard = self.editingGiftCard {
            self.giftCardNumberTextField.text = giftCard.giftCardNumber
            self.pinNumberTextField.text = giftCard.pinNumber
            self.currentBalanceTextField.text = (NSString(format: "%.02f", giftCard.currentBalance)) as String
            self.storeNameTextField.text = giftCard.storeName
            
        }
        
        self.giftCardNumberTextField.delegate = self
        self.pinNumberTextField.delegate = self
        self.currentBalanceTextField.delegate = self
        self.storeNameTextField.delegate = self
        
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissAddViewController(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func saveNewCard(sender: AnyObject) {
        if let giftCard: GiftCard = self.editingGiftCard {
            giftCard.giftCardNumber = self.giftCardNumberTextField.text!
            giftCard.pinNumber = self.pinNumberTextField.text!
            giftCard.currentBalance = (self.currentBalanceTextField.text! as NSString).floatValue
            giftCard.storeName = self.storeNameTextField.text!
            giftCard.photo = UIImage(named: giftCard.storeName)
            self.delegate?.userDidSave(giftCard)
        } else {
            let newGiftCard: GiftCard = GiftCard(giftCardNumber: "", pinNumber: "", currentBalance: 0.00, storeName: "")
            newGiftCard.giftCardNumber = giftCardNumberTextField.text!
            newGiftCard.pinNumber = pinNumberTextField.text
            newGiftCard.currentBalance = (currentBalanceTextField.text! as NSString).floatValue
            newGiftCard.storeName = storeNameTextField.text!
            newGiftCard.photo = UIImage(named: newGiftCard.storeName)
            self.delegate?.userDidSave(newGiftCard)
        }
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }

}

extension AddEditGiftCardViewController: UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.giftCardNumberTextField {
            self.pinNumberTextField.becomeFirstResponder()
        } else if textField == self.pinNumberTextField {
            self.currentBalanceTextField.becomeFirstResponder()
        } else if textField == self.currentBalanceTextField {
            self.storeNameTextField.becomeFirstResponder()
        } else if textField == self.storeNameTextField {
            self.storeNameTextField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if textField.text == nil || textField.text!.isEmpty {
            return false
        } else {
            if textField == self.giftCardNumberTextField && textField.text!.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) != 16 {
                return false
            } else if textField == self.pinNumberTextField && textField.text!.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) != 4 {
                    return false
            }
                return true
        }
  
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == self.giftCardNumberTextField {
            if let range = string.rangeOfString("[\\D]", options: NSStringCompareOptions.RegularExpressionSearch) {
                return false
            }
        if textField == self.currentBalanceTextField {
            if let range = string.rangeOfString("[\\D]", options: NSStringCompareOptions.RegularExpressionSearch) {
                    
            }
            }
    }
        return true
    }
    
}

extension AddEditGiftCardViewController : UIPickerViewDelegate {
    
}

extension AddEditGiftCardViewController : UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
}
