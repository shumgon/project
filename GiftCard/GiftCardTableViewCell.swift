//
//  GiftCardTableViewCell.swift
//  GiftCard
//
//  Created by Santia Muthusamy on 4/9/16.
//  Copyright © 2016 Qi Xiong Ng. All rights reserved.
//

import UIKit

class GiftCardTableViewCell: UITableViewCell {

    var giftCard: GiftCard! {
        didSet {
            updateDisplay()
        }
    }
    
    func updateDisplay() {
        giftCardNumberLabel.text = self.giftCard.giftCardNumber
        currentBalanceLabel.text = "$ " + ((NSString(format: "%.02f", self.giftCard.currentBalance)) as String)
        merchantPictures.image = self.giftCard.photo
    }
    
    @IBOutlet weak var giftCardNumberLabel: UILabel!
    @IBOutlet weak var currentBalanceLabel: UILabel!
    @IBOutlet weak var merchantPictures: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
