//
//  showGiftCardViewController.swift
//  GiftCard
//
//  Created by Santia Muthusamy on 4/6/16.
//  Copyright © 2016 Qi Xiong Ng. All rights reserved.
//

import UIKit

class ShowGiftCardDetailsViewController: UIViewController {

    @IBOutlet weak var giftCardNumberLabel: UILabel!
    @IBOutlet weak var pinNumberLabel: UILabel!
    @IBOutlet weak var currentBalanceLabel: UILabel!
    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    var giftCard: GiftCard?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateDisplay()
        
    }
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateDisplay() {
        giftCardNumberLabel.text = "Gift Card No : " + giftCard!.giftCardNumber
        if let pinNumber = giftCard!.pinNumber {
            pinNumberLabel.text = "Pin No : " + pinNumber
        } else {
            pinNumberLabel.text = "Pin No : -"
        }
        currentBalanceLabel.text = "Balance : $ " + ((NSString(format: "%.02f", self.giftCard!.currentBalance)) as String)
        storeNameLabel.text = "Store Name : " + giftCard!.storeName

    }
    
    override func viewDidDisappear(animated: Bool) {
        updateDisplay()
    }

    @IBAction func editGiftCardDetails(sender: ShowGiftCardDetailsViewController) {
        let editView: AddEditGiftCardViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddEditGiftCardViewController") as! AddEditGiftCardViewController
        editView.delegate = self
        editView.editingGiftCard = giftCard
        self.presentViewController(editView, animated: true, completion: nil)

    }

}

extension ShowGiftCardDetailsViewController:AddEditGiftCardViewControllerDelegate {
    func userDidSave(giftcard: GiftCard) {
        updateDisplay()
        
//        GiftCardLoader.sharedLoader.editGiftCard(giftcard)
        GiftCardLoader.sharedLoader.saveGiftCardsToFile()
    }
}
