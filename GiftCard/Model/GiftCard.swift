//
//  GiftCard.swift
//  GiftCard
//
//  Created by Santia Muthusamy on 4/9/16.
//  Copyright © 2016 Qi Xiong Ng. All rights reserved.
//

import Foundation
import UIKit

class GiftCard: NSObject, NSCoding {
    
    var giftCardNumber: String
    var pinNumber: String?
    var currentBalance: Float
    var storeName: String
    var photo: UIImage?
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        keyedArchiver.encodeObject(self.giftCardNumber, forKey: "GCGiftCardgiftCardNumber")
        keyedArchiver.encodeObject(self.pinNumber, forKey: "GCGiftCardpinNumber")
        keyedArchiver.encodeObject(self.currentBalance, forKey: "GCGiftCardcurrentBalance")
        keyedArchiver.encodeObject(self.storeName, forKey: "GCGiftCardstoreName")
        
        if self.photo != nil {
            let imageData: NSData? = UIImagePNGRepresentation(self.photo!)
            keyedArchiver.encodeObject(imageData, forKey: "GCGiftCardphoto")
        }
    }
    
    required convenience init? (coder aDecoder:NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        let giftCardNumber: String = keyedUnarchiver.decodeObjectForKey("GCGiftCardgiftCardNumber") as! String
        let pinNumber: String? = keyedUnarchiver.decodeObjectForKey("GCGiftCardpinNumber") as? String
        let currentBalance: Float = keyedUnarchiver.decodeObjectForKey("GCGiftCardcurrentBalance") as! Float
        let storeName: String = keyedUnarchiver.decodeObjectForKey("GCGiftCardstoreName") as! String
        self.init(giftCardNumber: giftCardNumber, pinNumber: pinNumber, currentBalance: currentBalance, storeName: storeName)
        
        if let imageData = keyedUnarchiver.decodeObjectForKey("GCGiftCardphoto") as? NSData {
            self.photo = UIImage(data: imageData)
        }
        
        
    }
    init(giftCardNumber: String, pinNumber: String? = nil, currentBalance: Float, storeName: String) {
        self.giftCardNumber = giftCardNumber
        self.pinNumber = pinNumber
        self.currentBalance = currentBalance
        self.storeName = storeName
        
        super.init()
    }
}
